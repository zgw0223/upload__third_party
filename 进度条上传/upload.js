//上传文件方法
var xhr;
var ot;
var oloaded;
function UpladFile(url) { //url 为上传的接口地址
    var fileObj = document.getElementById("music").files[0]; // music为被定义的上传input的ID名
    var form = new FormData(); // FormData 对象
    form.append("music", fileObj); // 文件对象  传递的参数名
    xhr = new XMLHttpRequest();  // XMLHttpRequest 对象
    xhr.open("post", url, true); //post方式，url为服务器请求地址，true 该参数规定请求是否异步处理。
    //xhr.onload = uploadComplete; //请求完成
    //xhr.onerror =  uploadFailed; //请求失败
    xhr.upload.onprogress = progressFunction;//【上传进度调用方法实现】
    xhr.upload.onloadstart = function(){//上传开始执行方法
        ot = new Date().getTime();   //设置上传开始时间
        oloaded = 0;//设置上传开始时，以上传的文件大小为0
    };
    xhr.send(form); //开始上传，发送form数据
    xhr.onreadystatechange = function() {
    	//console.log(xhr.readyState);
    	//console.log(xhr.status)
		if(xhr.readyState == 4 && xhr.status == 200) {
			console.log('成功');
			var obj= JSON.parse(xhr.responseText);
			console.log(obj);
			if(obj.Code==0){
				
			}else{
				console.log('成功2'); 
			}
		};
	};
	//上传进度实现方法，上传过程中会频繁调用该方法
	function progressFunction(evt) {
	     var progressBar = document.getElementById("progressBar");
	     var percentageDiv = document.getElementById("percentage");
	     // event.total是需要传输的总字节，event.loaded是已经传输的字节。如果event.lengthComputable不为真，则event.total等于0
	     if (evt.lengthComputable) {
	         progressBar.max = evt.total;
	         progressBar.value = evt.loaded;
	         percentageDiv.innerHTML = Math.round(evt.loaded / evt.total * 100) + "%";
	     }
	    
	    var time = document.getElementById("time");
	    var nt = new Date().getTime();//获取当前时间
	    var pertime = (nt-ot)/1000; //计算出上次调用该方法时到现在的时间差，单位为s
	    ot = new Date().getTime(); //重新赋值时间，用于下次计算
	    
	    var perload = evt.loaded - oloaded; //计算该分段上传的文件大小，单位b       
	    oloaded = evt.loaded;//重新赋值已上传文件大小，用以下次计算
	
	    //上传速度计算
	    var speed = perload/pertime;		//单位b/s
	    var bspeed = speed;
	    var units = 'b/s';//单位名称
	    if(speed/1024>1){
	        speed = speed/1024;
	        units = 'k/s';
	    }
	    if(speed/1024>1){
	        speed = speed/1024;
	        units = 'M/s';
	    }
	    speed = speed.toFixed(1);
	    //剩余时间
	    var resttime = ((evt.total-evt.loaded)/bspeed).toFixed(1);
	    time.innerHTML = '，速度：'+speed+units+'，剩余时间：'+resttime+'s';
	       if(bspeed==0)
	        time.innerHTML = '上传已取消';
	}
	//上传成功响应
	//function uploadComplete(evt) {
	 //服务断接收完文件返回的结果
	 //    alert(evt.target.responseText);
	     //alert("上传成功！");
	//}
	//上传失败
	//function uploadFailed(evt) {
	    //alert("上传失败！");
	//}
	//取消上传
};
function cancleUploadFile(){			//取消上传
    xhr.abort();
};
var music_nav='<div class="jindutiao" style="width: 60%;height:50px;line-height:20px;margin:10px auto;">'+
				'<progress id="progressBar" value="0" max="100" style="width: 300px;"></progress>'+
		    	'<span id="percentage"></span><span id="time"></span>'+
			'</div>';  //music_nav进度条展示  append到页面中 样式也可以自己更改  id跟上面一致就行
//列：
//上传音频文件,进度条展示
$("#music_upload").click(function(){				//layer需要引用layer.js
	var indexs=layer.confirm(music_nav, {
	  btn: ['取消'],   //按钮
	  title:"上传文件"
	}, function(){
		cancleUploadFile();
	  layer.msg('取消成功', { icon: 1, time: 1000 });
	});
	/*var indexs=layer.open({
	  type: 1,
	  title: false,
	  closeBtn: 0,
	  //shadeClose: true,
	  skin: 'yourclass',
	  content:music_nav,
	  btn: ['取消'] //按钮
	},function(){
		//cancleUploadFile();
		layer.msg('取消成功', { icon: 1, time: 1000 });
	});*/
	UpladFile("/admin/admin.php?path=uploadMusic");
});
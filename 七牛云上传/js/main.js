var token;//存储我们从后台获取的token
var url = "http://zhima.php.jxcraft.net/public/index.php/index/Qiniu/getToken"   //这里填写ajax请求地址
var domain = "http://pachc5x50.bkt.clouddn.com/"    //这里填写七牛下你绑定的域名（会有一个默认域名）

//页面加载就开始获取token,在获取token的ajax中，如果获取token成功，就开始上传文件！
$(document).ready(function () {
    //获取token
    getToken();

    //初始化上传程序
    //UpLoadImg(“选择文件按钮的ID”，“上传成功后展示信息的ID”，“上传成功后展示文件大小区域的ID”，“上传文件成功后展示文件名字的ID”)
    UpLoadImg('pickfiles1', 'container1', '#msg1', '#Size1', '#fileName1');
});

//获取token
function getToken() {
    $.ajax({
        type: "GET",
        url: url,
        async:false,
        success: function(data) {
            token = JSON.parse(data).data.token;
        },
        error: function() {},
        beforeSend: function() {}
    });
    return token;
}

//UpLoadImg(“选择文件按钮的ID”，“上传成功后展示信息的ID”，“上传成功后展示文件大小区域的ID”，“上传文件成功后展示文件名字的ID”)
function UpLoadImg(button,father,msg,size,fileName)
{
   var uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4', //上传模式,依次退化
        browse_button: button, //上传选择的点选按钮，**必需**
        //uptoken_url: '/token',            //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
        uptoken: token, //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
        // unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
        // save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
        domain: domain, //bucket 域名，下载资源时用到，**必需**
        get_new_uptoken: false, //设置上传文件的时候是否每次都重新获取新的token
        container: father, //上传区域DOM ID，默认是browser_button的父元素，
        max_file_size: '100mb', //最大文件体积限制
        flash_swf_url: 'js/plupload/Moxie.swf', //引入flash,相对路径
        max_retries: 3, //上传失败最大重试次数
        dragdrop: true, //开启可拖曳上传
        drop_element: 'container1', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
        chunk_size: '4mb', //分块上传时，每片的体积
        auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
        filters: {		//限制文件格式
          // Specify what files to browse for
             mime_types: [
                 {title : 'mp4 files', extensions : 'mp4'} // 限定apk后缀上传格式上传
             ]
        },
        init: {
            'FilesAdded': function (up, files) {
                plupload.each(files, function (file) {
                    // 文件添加进队列后,处理相关的事情
                    //if (checkImg(file.name) == false)
                    //{ 
                    //    alert("图片类型必须是.gif,jpeg,jpg,png中的一种");
                    //    return;
                    console.log("添加队列");
                    var testImgSrc = file.getNative();
                });
            },
            'BeforeUpload': function (up, file) {
                // 每个文件上传前,处理相关的事情
                console.log("上传前");
            },
            'UploadProgress': function (up, file) {
                // 每个文件上传时,处理相关的事情

                console.log("上传时");
                $(msg).text('正在上传···');
            },
            'FileUploaded': function (up, file, info) {
                // 每个文件上传成功后,处理相关的事情
                // 其中 info 是文件上传成功后，服务端返回的json，形式如
                // {
                //    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
                //    "key": "gogopher.jpg"
                //  }
                // 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
                console.log(info)
                var domain = up.getOption('domain');
                var res = JSON.parse(info.response);
                console.log(res)
                //var res = info;
                var sourceLink = domain + res.key; //获取上传成功后的文件的Url

                console.log("上传成功：" + sourceLink);

                $(msg).text('上传成功!!');
                $(size).text(bytesToSize(file.size));
                $(fileName).text('文件名：' + file.name);

            },
            'Error': function (up, err, errTip) {
                //上传出错时,处理相关的事情
                console.log("上传出错");
                alert(err);
            },
            'UploadComplete': function () {
                //队列文件处理完毕后,处理相关的事情
            },
            'Key': function (up, file) {
                // 若想在前端对每个文件的key进行个性化处理，可以配置该函数
                // 该配置必须要在 unique_names: false , save_key: false 时才生效
                //var key = "";
                // do something with key here
                //return key
            }
        }
    });
}

//function checkImg(url) {
//  
//  if (!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(url)) {
//      return false;
//  }
//  else {
//      return true;
//  }
//}
//----------------------
//引入Plupload 、qiniu.js后
// domain 为七牛空间（bucket)对应的域名，选择某个空间后，可通过"空间设置->基本设置->域名设置"查看获取


//转换大小
function bytesToSize(bytes) {
    if (bytes === 0) return '0 B';
    var k = 1024;
    sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    i = Math.floor(Math.log(bytes) / Math.log(k));
    return (Math.ceil(bytes / Math.pow(k, i))) + ' ' + sizes[i];
    //toPrecision(3) 后面保留一位小数，如1.0GB                                                                                                                  //return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];  
}
